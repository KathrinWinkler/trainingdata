package preprocessing;
/* 
 * for files of type 2018-04-11T13-42-33.511Z-labelled_33L.csv
format:
timestamp,acc_raw,truth
2018-04-11T13-42-33.480000Z,565,0
2018-04-11T13-42-33.513000Z,564,0
2018-04-11T13-42-33.546000Z,565,0
*/

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class LabeledRawDataToTrainingData  {
	private static String labeledRawDataFile; //2018-04-24T12-08-54.000Z-labelled.csv
	private static int windowLength = 256;
	private static String treningDataFile;
	
	//private Series<Number, Number> ssSeries1 = new XYChart.Series<>();
	//private Series<Number, Number> ssSeries2 = new XYChart.Series<>();
	
	//this is for entries within the signal strength file
	private static DateTimeFormatter formatterSs = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss'Z'");//2018-04-08T10-00-00Z
	private static DateTimeFormatter formatterRaw = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss.SSS"); //2018-04-19T19-05-02.469000Z
	private static ZoneId zoneIdUTC = ZoneId.of("Z"); //UTC
	private static ZoneId zoneIdOslo = ZoneId.of("Europe/Oslo");
	
	
	
	public static void main(String[] args) {
		parseCommandLineArgs(args);
		generateTrainingData();
	}
	
	private static void parseCommandLineArgs(String[] args) {
		Options options = new Options();
		
		Option labeledRawDataFileOption = Option.builder("lrd")
				.longOpt("labeledRawData")
				.required()
				.desc("labeled raw data csv file (timestamp, acceleration, groundTruth)")
				.hasArg()
				.argName("labledRawDAta").build();
		options.addOption(labeledRawDataFileOption);
		Option windowLengthOption = Option.builder("wl")
				.longOpt("windowLength")
				.required()
				.desc("window length")
				.hasArg()
				.argName("windowLength").build();
		options.addOption(windowLengthOption);
		
		
		try {
			CommandLine cmd = new DefaultParser().parse(options, args);
			
			labeledRawDataFile = cmd.getOptionValue("lrd");
			windowLength = Integer.parseInt(cmd.getOptionValue("wl"));
			int lastDot = labeledRawDataFile.lastIndexOf('.');
			treningDataFile = labeledRawDataFile.substring(0,lastDot) + "_" + windowLength + labeledRawDataFile.substring(lastDot);
			
			
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("labeledrawdatatotrainingdata", options);
			System.exit(1);
		}

	}
	
	private static void generateTrainingData() {
		//read data
		List<CSVRecord> csvListMurataRaw = readLabeledRawData(labeledRawDataFile);
		//make training samples
		exportSampleWindows(csvListMurataRaw);
		
		
		
	}
	
	
	
	private static List<CSVRecord> readLabeledRawData(String fileName){
		System.out.println("read from: filename = " + fileName);
		try (CSVParser csvParser = new CSVParser(Files.newBufferedReader(Paths.get(fileName)),
				CSVFormat.DEFAULT.withHeader("timestamp","acc_raw","truth"))) {
			List<CSVRecord> records = csvParser.getRecords();
			return records;
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	private static void exportSampleWindows(List<CSVRecord> csvListMurataRaw){
		BufferedWriter fileWriter;
		CSVPrinter csvPrinter;
		try {
			fileWriter = Files.newBufferedWriter(Paths.get(treningDataFile));

			csvPrinter = new CSVPrinter(fileWriter, 
					CSVFormat.DEFAULT.withEscape('\\').withDelimiter(',').withQuoteMode(QuoteMode.NONE));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		int samples_used = 0;
		int samples_not_used = 0;

		List<String> murataRawValues = new ArrayList<String>(windowLength + 1);
		List<Integer> truthAtThisTime = new ArrayList<Integer>(windowLength);
		for(int line_number = 1; line_number < csvListMurataRaw.size()-windowLength; line_number=line_number+64) {
			murataRawValues.clear();
			truthAtThisTime.clear();
			for(int j = 0; j < windowLength; j++) {
				String rawValue = csvListMurataRaw.get(line_number+j).get(1);
				murataRawValues.add(rawValue);
				int truth = Integer.parseInt(csvListMurataRaw.get(line_number+j).get(2));
				truthAtThisTime.add(truth);
			}
			//use this window only if truth is consitent
			if(isAllEqual(truthAtThisTime)) {
				//use this window
				String truthForThisWindow = Integer.toString(truthAtThisTime.get(0));
				murataRawValues.add(truthForThisWindow);
				try {
					csvPrinter.printRecord(murataRawValues);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				samples_used++;
			}
			else {
				System.out.println("cannot use this sample due to inconsitent ground truth");
				samples_not_used++;
			}
		}
		try {
			csvPrinter.flush();
			csvPrinter.close();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("used: " + samples_used);
		System.out.println("not used: "+ samples_not_used);
	}
	
	private static boolean isAllEqual(List<Integer> list) {
		boolean allEqual = list.stream().distinct().limit(2).count() <= 1;
		return allEqual;
	}
	
	
	
	
	
	
}
