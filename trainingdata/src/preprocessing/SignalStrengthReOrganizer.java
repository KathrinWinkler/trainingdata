package preprocessing;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class SignalStrengthReOrganizer  {
	private static String muratafile1;
	private static String muratafile2;
	
	//private Series<Number, Number> ssSeries1 = new XYChart.Series<>();
	//private Series<Number, Number> ssSeries2 = new XYChart.Series<>();
	
	//this is for entries within the signal strength file
	private static DateTimeFormatter formatterSs = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss'Z'");//2018-04-08T10-00-00Z
	private static ZoneId zoneIdUTC = ZoneId.of("Z"); //UTC
	private static ZoneId zoneIdOslo = ZoneId.of("Europe/Oslo");
	
	//this is for the signal strength file name:
	private static DateTimeFormatter fileNameFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH'Z'"); //2018-04-08T
	//bedsensor-00AEFA36C2B5-2018-04-08T10Z.csv
	
	public static void main(String[] args) {
		parseCommandLineArgs(args);
		reorganize();
	}
	
	private static void parseCommandLineArgs(String[] args) {
		Options options = new Options();
		
		Option murataFileOption1 = Option.builder("m1")
				.longOpt("murata1")
				.required()
				.desc("The csv log-file 1 for bed sensor readings (signal strength).")
				.hasArg()
				.argName("muratafile1").build();
		options.addOption(murataFileOption1);
		Option murataFileOption2 = Option.builder("m2")
				.longOpt("murata2")
				.required()
				.desc("The csv log-file 2 for bed sensor readings (signal strength).")
				.hasArg()
				.argName("muratafile2").build();
		options.addOption(murataFileOption2);
		
		try {
			CommandLine cmd = new DefaultParser().parse(options, args);
			
			muratafile1 = cmd.getOptionValue("m1");
			muratafile2 = cmd.getOptionValue("m2");
			//TODO: assert that these files cover 2 successive days!
			//parse file name for date to check
			
			
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("signalstrengthreorganizer", options);
			System.exit(1);
		}

	}
	
	private static void reorganize() {
		//use data from 2 signal strength files , where each file covers data from 12-12
		//to generate 1 file covering 0-24
		ZonedDateTime zdt1 = extractStartDateTimeFromFileName(muratafile1);
		ZonedDateTime zdt2 = extractStartDateTimeFromFileName(muratafile2);
		System.out.println("start time first file: "+ zdt1);
		System.out.println("start time second file: "+ zdt2);
		assert(zdt1.isBefore(zdt2));
		ZonedDateTime midnightZdt = zdt2.toLocalDate().atStartOfDay(zoneIdOslo); //sollte das nicht Oslo sein statt UTC??
		System.out.println("midnight: " + midnightZdt);
		ZonedDateTime midnightZdt2 = midnightZdt.plus(Period.ofDays(1));
		System.out.println("midnight2 : " + midnightZdt2);
		
		//first file: read from midnight:
		long midnightInEpochSeconds = midnightZdt.toEpochSecond();
		long midnightInEpochSeconds2 = midnightZdt2.toEpochSecond();
		List<CSVRecord>  csvList1 = readMurataFileFromTime(muratafile1, midnightInEpochSeconds);
		//second file: read until midnight:
		List<CSVRecord> csvList2= readMurataFileUntilTime(muratafile2, midnightInEpochSeconds2);
		List<CSVRecord> resultList = new ArrayList<CSVRecord>();
		resultList.addAll(csvList1);
		resultList.addAll(csvList2);
		//finished here?
		//just write to csv file?
		
		int lastDot = muratafile2.lastIndexOf('.');
		String outfile = muratafile2.substring(0,lastDot) + "_00" + muratafile2.substring(lastDot);
		
		
		BufferedWriter writer;
		try {
			writer = Files.newBufferedWriter(Paths.get(outfile));
			CSVPrinter csvPrinter = new CSVPrinter(writer, 
					CSVFormat.DEFAULT.withHeader("timestamp","counter","heartrate","respirationrate","strokevolume",
							"heartratevariability","signalstrength","status","b2b","b2b_1","b2b_2"));
			csvPrinter.printRecords(resultList);
			csvPrinter.flush();
			csvPrinter.close();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//or convert to series?
		//Series<Number, Number> resultSeries = new XYChart.Series<>();
		//resultSeries.getData().addAll(resultList);
	}
	
	
	private static ZonedDateTime extractStartDateTimeFromFileName(String fileName) {
		System.out.println("fileName: " + fileName);
		System.out.println("fileNameFormatter: " + fileNameFormatter);
		//find date information within fileName string
		Pattern pattern = Pattern.compile(".*bedsensor.*(\\d{4}-\\d{2}-\\d{2}T\\d{2}Z).*"); ///...../bedsensor-00AEFA36C2B5-2018-04-08T10Z.csv
		Matcher matcher = pattern.matcher(fileName);
		String dateInfo = "";
		if(matcher.find()) {
			dateInfo = matcher.group(1);
			System.out.println("dateInfo from filename: "+ dateInfo);
		}
		else {
			System.out.println("something went wrong parsing the file name");
		}
		
		LocalDateTime ldt = LocalDateTime.parse(dateInfo, fileNameFormatter); 
		ZonedDateTime zdt = ZonedDateTime.ofLocal(ldt, zoneIdUTC, ZoneOffset.ofHours(2));//this file is UTC, so we need to adjust
		return zdt; 
	}
	
	
	
	//private static Series<Number, Number> readMurataFileFromTime(String fileName,  long epochSecondsLimiter){
	private static List<CSVRecord>  readMurataFileFromTime(String fileName,  long epochSecondsLimiter){
		System.out.println("read from: filename = " + fileName);
		try (CSVParser csvParser = new CSVParser(Files.newBufferedReader(Paths.get(fileName)),
				CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			List<CSVRecord> records = csvParser.getRecords();
			List<CSVRecord> selectedRecords = new ArrayList<CSVRecord>();
			for (CSVRecord rr : records) {
				long epochSecond = timestampToEpochSecondMurata(rr.get(0));
				if(epochSecond >= epochSecondsLimiter) {
					selectedRecords.add(rr);
					/*
					int currentSs = Integer.parseInt(rr.get("signalstrength"));
					
					ssList.add(new XYChart.Data<Number, Number>(epochSecond, currentSs));	
					if(epochSecond == previousEpochSecond) {
						//System.out.println("WARNING!!");
					}
					previousEpochSecond = epochSecond;
					*/
				}
			}
			return selectedRecords;
			/*
			Series<Number, Number> ssSeries = new XYChart.Series<>();
			ssSeries.getData().addAll(ssList);
			System.out.println("Read From Time: Number of signal strength entries: " + ssList.size());
			return ssSeries;
			*/
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
		
	}
	
	//private static Series<Number, Number> readMurataFileUntilTime(String fileName, long epochSecondsLimiter){
	private static List<CSVRecord>  readMurataFileUntilTime(String fileName, long epochSecondsLimiter){
		System.out.println("read until: filename = " + fileName);
		try (CSVParser csvParser = new CSVParser(Files.newBufferedReader(Paths.get(fileName)),
				CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			
			List<CSVRecord> records = csvParser.getRecords();
			List<CSVRecord> selectedRecords = new ArrayList<CSVRecord>();
			
			for (CSVRecord rr : records) {
				long epochSecond = timestampToEpochSecondMurata(rr.get(0));
				if(epochSecond < epochSecondsLimiter) {
					selectedRecords.add(rr);;
				//if(epochSecond >= epochSecondsLimiter) {
				//	break;
				//}
					/*
					int currentSs = Integer.parseInt(rr.get("signalstrength"));
				
					ssList.add(new XYChart.Data<Number, Number>(epochSecond, currentSs));	
					if(epochSecond == previousEpochSecond) {
						//System.out.println("WARNING!!");
					}
					previousEpochSecond = epochSecond;
					*/
				}
			}
			return selectedRecords;
			/*
			Series<Number, Number> ssSeries = new XYChart.Series<>();
			ssSeries.getData().addAll(ssList);
			System.out.println("Read Until Time: Number of signal strength entries: " + ssList.size());
			return ssSeries;
			*/
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
		
	}
	
	private static long timestampToEpochSecondMurata(String timestamp) {
		return timestampToZonedDateTimeMurata(timestamp).toEpochSecond();
	}
	
	private static ZonedDateTime timestampToZonedDateTimeMurata(String timestamp) {
		//return ZonedDateTime.parse(timestamp + " +02:00", formatterSs);
		LocalDateTime ldt = LocalDateTime.parse(timestamp, formatterSs);
		ZonedDateTime zdt = ZonedDateTime.ofLocal(ldt, zoneIdUTC, ZoneOffset.ofHours(2));//this file is UTC, so we need to adjust
		return zdt; 
		
	}
}
